package tdd.training.bsk;

public class Frame {

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	private int firstThrow;
	private int secondThrow;
	private int bonus = 0;
	
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		validPoint(firstThrow);
		validPoint(secondThrow);
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
	}
	
	private void validPoint(int point) throws BowlingException{
		if( point <0 || point >10) {
			throw new BowlingException("Valori inseriti non validi"); 
		}
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) throws BowlingException{
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		return firstThrow + secondThrow + bonus;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return firstThrow == 10;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		Frame frameComparison = (Frame) obj;
		return frameComparison.getSecondThrow() == this.secondThrow && frameComparison.getFirstThrow() == this.firstThrow;
	}
	
	
	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		if(!isStrike()) {
			return (firstThrow + secondThrow) == 10;
		}
		return false;
	}

}
