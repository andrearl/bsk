package tdd.training.bsk;

import java.util.ArrayList;


public class Game {

	/**
	 * It initializes an empty bowling game.
	 */
	private ArrayList<Frame> framesGame;
	private int firstThrowBonus;
	private int secondThrowBonus;
	
	public Game() {
		framesGame = new ArrayList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		framesGame.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return framesGame.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		Frame lastFrame = framesGame.get(framesGame.size()-1);
		if(!lastFrame.isSpare() && !lastFrame.isStrike()) {
			throw new BowlingException("L'ultimo frame non era uno spare!");
		}
		this.firstThrowBonus = firstBonusThrow;
		lastFrame.setBonus(firstThrowBonus);
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		Frame lastFrame = framesGame.get(framesGame.size()-1);
		if(!lastFrame.isStrike()) {
			throw new BowlingException("L'ultimo frame non era uno strike!");
		}
		this.secondThrowBonus = secondBonusThrow;
		lastFrame.setBonus(secondThrowBonus + firstThrowBonus);
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstThrowBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondThrowBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for (int i = 0; i < framesGame.size(); i++) {
			Frame frame = framesGame.get(i);
			if(i < framesGame.size()-1) {
				setBonus(frame, i);
			} 
			score = score + frame.getScore();
		}
		return score;	
	}
	
	private void setBonus(Frame frame, int index) throws BowlingException{
		if(frame.isSpare()) {
			frame.setBonus(framesGame.get(index+1).getFirstThrow());
		} else if(frame.isStrike()) {
			if(framesGame.get(index+1).isStrike()) {
				lastStrike(index+1, frame);
			}else {
				frame.setBonus(framesGame.get(index+1).getFirstThrow() + framesGame.get(index+1).getSecondThrow());
			}
		}
	}
	
	private void lastStrike(int index, Frame frame) throws BowlingException{
		Frame lastStrike = framesGame.get(index);
		if(index < framesGame.size()-2) {
			frame.setBonus(lastStrike.getFirstThrow() + framesGame.get(index+1).getFirstThrow());
		} else {
			frame.setBonus(lastStrike.getFirstThrow() + firstThrowBonus);
		}
	}

	public void updateFrame(int i, Frame frame) {
		framesGame.set(i, frame);
	}

}
