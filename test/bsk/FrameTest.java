package bsk;

import static org.junit.Assert.*;

import org.junit.Test;
import tdd.training.bsk.*;


public class FrameTest {

	@Test
	public void testFirstThrowShouldBeInsert() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(2,frame.getFirstThrow());
	}

	@Test
	public void testSecondThrowShouldBeInsert() throws Exception{
		Frame frame = new Frame(2,4);
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test(expected = BowlingException.class)
	public void testFrameShouldNotBeCreated() throws Exception{
		new Frame(-2, 4);
	}
	
	@Test
	public void testScoreShouldBeTHeSumOfTheTwoThrows() throws Exception{
		Frame frame = new Frame(2,6);
		assertEquals(8,frame.getScore());
	}
	
	@Test
	public void testFrameShouldBeSpare() throws Exception{
		Frame frame = new Frame(1,9);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testFrameShouldHaveBonus() throws Exception{
		Frame frame = new Frame(1,9);
		frame.setBonus(3);
		assertEquals(3,frame.getBonus());
	}
	
	@Test
	public void testFrameShouldCalculateScoreWithBonus() throws Exception{
		Frame frame = new Frame(1,9);
		frame.setBonus(3);
		assertEquals(13,frame.getScore());
	}
	
	@Test
	public void testFrameShouldBeStrike() throws Exception{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testFrameShouldCalculateScoreWithStrike() throws Exception{
		Frame frame = new Frame(10,0);
		frame.setBonus(9);
		assertEquals(19,frame.getScore());
	}
}
