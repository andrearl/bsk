package bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.training.bsk.*;


public class GameTest {
	
	private Game game;
	
	@Before
	public void setUpGame() throws Exception{
		game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
	}

	@Test
	public void testFirstFrameShouldBeInsertInGame() throws Exception{
		Frame frame = new Frame(2,6);
		assertTrue(frame.equals(game.getFrameAt(9)));
	}

	@Test
	public void testScoreShouldBeSumOfAllFrame() throws Exception{
		assertEquals(81,game.calculateScore());
	}
	
	@Test
	public void testFirstFrameShouldBeUpdate() throws Exception{
		Frame frame = new Frame(1,9);
		game.updateFrame(0,frame);
		assertTrue(frame.equals(game.getFrameAt(0)));
	}
	
	@Test
	public void testCalculateScoreWithSpareFrame() throws Exception{
		game.updateFrame(0, new Frame(1,9));
		assertEquals(88,game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeFrame() throws Exception{
		game.updateFrame(0, new Frame(10,0));
		assertEquals(94,game.calculateScore());
	}
	
	@Test
	public void testCalculateSpareAfterStrike() throws Exception{
		game.updateFrame(0, new Frame (10,0));
		game.updateFrame(1, new Frame (4,6));
		assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testCalculateStrikeAfterStrike() throws Exception{
		game.updateFrame(0, new Frame (10,0));
		game.updateFrame(1, new Frame (10,0));
		assertEquals(112,game.calculateScore());
	}
	
	@Test
	public void testCalculateSpareAfterSpare() throws Exception{
		game.updateFrame(0, new Frame (8,2));
		game.updateFrame(1, new Frame (5,5));
		assertEquals(98,game.calculateScore());
	}
	
	@Test
	public void testBonusThrowAfterSpareShouldBeSet() throws Exception{
		game.updateFrame(9, new Frame (2,8));
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void testCalculateScoreWithBonusThrowAfterSpare() throws Exception{
		game.updateFrame(9, new Frame (2,8));
		game.setFirstBonusThrow(7);
		assertEquals(90, game.calculateScore());
	}
	
	@Test(expected = BowlingException.class)
	public void testSpareBonusThrowShouldNotBeSet() throws Exception{
		game.setFirstBonusThrow(7);
	}
	
	@Test
	public void testBonusThrowAfterStrikeShouldBeSet() throws Exception{
		game.updateFrame(9, new Frame (10,0));
		game.setSecondBonusThrow(9);
		assertEquals(9, game.getSecondBonusThrow());
	}
	
	@Test(expected = BowlingException.class)
	public void testStrikeBonusThrowShouldNotBeSet() throws Exception{
		game.setSecondBonusThrow(9);
	}
	
	@Test
	public void testCalculateScoreWithBonusThrowAfterStrike() throws Exception{
		game.updateFrame(9, new Frame (10,0));
		game.setSecondBonusThrow(9);
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testCalculateAllStrike() throws Exception{
		for(int i = 0; i < 10; i++) {
			game.updateFrame(i, new Frame (10,0));
		}
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300, game.calculateScore());
	}
}
